import json
from glob import glob
from typing import Collection, Iterable, Iterator, List, Tuple

from torch import distributed


def iter_jsonl(glob_pattern: str) -> Iterator[dict]:
    for filename in glob(glob_pattern):
        with open(filename) as file:
            yield from map(json.loads, file)


def get_distributed_world_info() -> Tuple[int, int]:
    """Get information about distributed configuration.

    Returns:
        Tuple[int, int]: Number of distributed replicas and local rank id.
    """
    if distributed.is_available() and distributed.is_initialized():
        num_replicas, rank = distributed.get_world_size(), distributed.get_rank()
    else:
        num_replicas, rank = 1, 0

    return num_replicas, rank


def pad_sequence(seqs: Collection[Collection[int]]) -> List[List[int]]:
    """Pad sequences in order to obtain fixed array.
    Args:
        seqs (Collection[Collection[int]]): input sequences with different lengths.
    Returns:
        Collection[Collection[int]]: output sequence with equal lengths.
    """
    seq_lens = [len(s) for s in seqs]
    max_len = max(seq_lens)
    seqs = [seq + [0 for _ in range(max_len - len(seq))] for seq in seqs]

    return seqs


def count_lines(filename):
    """Count file lines.
    Args:
        filename (str): filename.
    Returns:
        int: number of lines.
    """

    def iter_blocks(file, size=65536):
        while True:
            b = file.read(size)
            if not b:
                break
            yield b

    with open(filename, encoding="utf-8", errors="ignore") as file:
        return sum(block.count("\n") for block in iter_blocks(file))


def batchify(iterable: Iterable, batch_size: int) -> Iterator[Tuple]:
    return zip(*[iter(iterable)] * batch_size)
