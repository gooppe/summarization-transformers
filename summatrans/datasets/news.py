import json
import math
import os
import random
from itertools import islice
from typing import List, Tuple

import torch
import youtokentome
from torch.utils.data import Dataset, IterableDataset

from .utils import (
    batchify,
    count_lines,
    get_distributed_world_info,
    iter_jsonl,
    pad_sequence,
)


class NewsDataset(Dataset):
    def __init__(
        self,
        tokenizer: youtokentome.BPE,
        data_dir: str,
        part: str,
        max_seq_len: int,
    ):
        source_filename = os.path.join(data_dir, f"{part}.jsonl")
        self.tokenizer = tokenizer
        self.max_seq_len = max_seq_len

        if part.startswith("test"):
            # For test part load all data into memory
            self.num_replicas, self.rank = 1, 0
        else:
            self.num_replicas, self.rank = get_distributed_world_info()

        dataset_len = count_lines(source_filename)
        self.num_samples = int(math.ceil(dataset_len / self.num_replicas))
        self.total_size = self.num_samples * self.num_replicas

        indices = list(range(dataset_len))
        indices += indices[: (self.total_size - len(indices))]
        self.indices = set(indices[self.rank : self.total_size : self.num_replicas])
        self.data = []

        with open(source_filename) as file:
            for i, line in enumerate(file):
                if i in self.indices:
                    dump = json.loads(line)
                    sample = (dump["text"], dump["headline"])
                    self.data.append(sample)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        text, headline = self.data[i]
        text = self.tokenizer.encode(text)
        headline = self.tokenizer.encode(headline, bos=True, eos=True)

        return text[: self.max_seq_len], headline[: self.max_seq_len]

    def get_random_test_sample(
        self,
    ) -> Tuple[str, str, torch.LongTensor, torch.LongTensor]:
        index = random.randint(0, len(self) - 1)
        source, target = self.data[index]
        source_encoded, target_encoded = [torch.LongTensor([t]) for t in self[index]]
        return source, target, source_encoded, target_encoded

    def collate(
        self, batch: List[Tuple[List[int], List[int]]]
    ) -> [torch.LongTensor, torch.LongTensor]:
        # Filter empty sequences
        batch = [(s, t) for s, t in batch if s and t]
        source, target = zip(*batch)

        source = pad_sequence(source)
        target = pad_sequence(target)

        return torch.LongTensor(source), torch.LongTensor(target)


class IterableNewsDataset(IterableDataset):
    def __init__(
        self,
        tokenizer: youtokentome.BPE,
        data_dir: str,
        part: str,
        batch_size: int,
        max_seq_len: int,
    ):
        self.source_filename = os.path.join(data_dir, f"{part}.jsonl")
        self.tokenizer = tokenizer
        self.batch_size = batch_size
        self.bucket_size = batch_size * 100
        self.max_seq_len = max_seq_len

        if part.startswith("test"):
            # For test part load all data into memory
            self.num_replicas, self.rank = 1, 0
            self.test_data = list(iter_jsonl(self.source_filename))
        else:
            self.num_replicas, self.rank = get_distributed_world_info()

        self.len = count_lines(self.source_filename) // self.num_replicas

    def get_random_test_sample(
        self,
    ) -> Tuple[str, str, torch.LongTensor, torch.LongTensor]:
        if not hasattr(self, "test_data"):
            raise RuntimeError("Test sampling available only for preloaded test data")

        sample = self.test_data[random.randint(0, len(self.test_data) - 1)]
        tensors = [torch.LongTensor([t]) for t in self.__prepare_sample(sample)]
        return sample["text"], sample["headline"], *tensors

    def __prepare_sample(self, sample: object) -> dict:
        text, headline = sample["text"], sample["headline"]
        text = self.tokenizer.encode(text)
        headline = self.tokenizer.encode(headline, bos=True, eos=True)

        return text[: self.max_seq_len], headline[: self.max_seq_len]

    def __iter_rank_data(self):
        data = iter_jsonl(self.source_filename)
        data = map(self.__prepare_sample, data)
        yield from islice(data, self.rank, None, self.num_replicas)

    def __len__(self):
        return self.len

    def __iter__(self):
        data = self.__iter_rank_data()
        bucket = list(islice(data, self.bucket_size))
        bg_loader = []

        while True:
            random.shuffle(bucket)
            for batch in batchify(bucket, self.batch_size):
                yield batch
                bg_loader.extend(islice(data, self.batch_size))

            if len(bg_loader) == self.bucket_size:
                bucket = bg_loader
                bg_loader = []
            else:
                yield from batchify(bg_loader, self.batch_size)
                break

    def collate(
        self, batch: List[Tuple[List[int], List[int]]]
    ) -> [torch.LongTensor, torch.LongTensor]:
        # Filter empty sequences
        batch = [(s, t) for s, t in batch if s and t]
        source, target = zip(*batch)

        source = pad_sequence(source)
        target = pad_sequence(target)

        return torch.LongTensor(source), torch.LongTensor(target)
