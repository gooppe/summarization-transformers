from typing import Tuple

import torch
from summatrans.modules.attention import FullAttention
from torch import nn


class RNN(nn.Module):
    def __init__(
        self,
        encoder_embeddings: Tuple[int, int],
        decoder_embeddings: Tuple[int, int],
        shared_embeddings: bool = True,
        hidden_size: int = 512,
        num_layers: int = 6,
        max_seq_len: int = 512,
        dropout: float = 0.1,
        **kwargs,
    ):
        super().__init__()

        assert hidden_size == encoder_embeddings[1] == decoder_embeddings[1]

        if shared_embeddings:
            assert encoder_embeddings[0] == decoder_embeddings[0]
            self.enc_embs = nn.Embedding(*encoder_embeddings, padding_idx=0)
            self.dec_embs = self.enc_embs
        else:
            self.enc_embs = nn.Embedding(*encoder_embeddings, padding_idx=0)
            self.dec_embs = nn.Embedding(*decoder_embeddings, padding_idx=0)

        vocab_size = decoder_embeddings[0]
        args = (hidden_size, hidden_size, num_layers, True, True, dropout)
        self.encoder = nn.GRU(*args, bidirectional=True)
        self.decoder = nn.GRU(*args)
        self.proj = nn.Sequential(nn.Linear(2 * hidden_size, hidden_size), nn.GELU())
        self.attention = ScaledDotProductAttention(hidden_size)
        self.out_to_vocab = nn.Linear(hidden_size * 2, vocab_size)

        self.max_seq_len = max_seq_len
        self.hidden_size = hidden_size
        self.hparams = {
            "encoder_embeddings": encoder_embeddings,
            "decoder_embeddings": decoder_embeddings,
            "shared_embeddings": shared_embeddings,
            "hidden_size": hidden_size,
            "num_layers": num_layers,
            "max_seq_len": max_seq_len,
            "dropout": dropout,
        }

    def forward(
        self, source: torch.LongTensor, target: torch.LongTensor
    ) -> torch.FloatTensor:
        source_emb = self.enc_embs(source)
        target_emb = self.dec_embs(target)

        encoder_repr, h_enc = self.encoder(source_emb)
        encoder_repr = self.proj(encoder_repr)
        h_dec = h_enc[::2, ...].contiguous()
        decoder_repr, _ = self.decoder(target_emb, h_dec)
        attentive_repr = self.attention(decoder_repr, encoder_repr, encoder_repr)
        out_distr = self.out_to_vocab(torch.cat((decoder_repr, attentive_repr), dim=-1))

        return out_distr

    @torch.no_grad()
    def generate(
        self,
        source: torch.LongTensor,
        limit: int = None,
        bos_token: int = 1,
        eos_token: int = 2,
    ) -> torch.LongTensor:
        """Generate predicted sequence.
        Args:
            source (torch.LongTensor): source tensor of shape `batch_size, seq_len`.
            limit (int): generation limit.
            bos_token (int): begin of sequence token. Defaults to 1.
            eos_token (int): end of sequence token. Defaults to 2.
        Returns:
            torch.LongTensor: generated tensor of shape `batch_size, gen_len`.
        """
        if limit is None:
            limit = self.max_seq_len
        else:
            assert limit <= self.max_seq_len

        batch_size, _ = source.shape
        device = source.device

        source = source[:, : self.max_seq_len, ...]
        encoder_emb = self.enc_embs(source)
        encoder_repr, h_enc = self.encoder(encoder_emb)
        encoder_repr = self.proj(encoder_repr)
        h_dec = h_enc[::2, ...].contiguous()

        prediction = torch.full(
            (batch_size, 1), bos_token, dtype=torch.long, device=device
        )

        for i in range(1, limit):
            decoder_emb = self.dec_embs(prediction).narrow(1, -1, 1)
            decoder_repr, h_dec = self.decoder(decoder_emb, h_dec)
            attentive_repr = self.attention(decoder_repr, encoder_repr, encoder_repr)
            dist = self.out_to_vocab(torch.cat((decoder_repr, attentive_repr), dim=-1))
            prediction = torch.cat((prediction, dist.argmax(-1)), dim=1)

            if torch.sum(prediction == eos_token, dim=1, dtype=torch.bool).all():
                break

        return prediction

    @staticmethod
    def criterion(
        prediction: torch.FloatTensor, target: torch.LongTensor
    ) -> torch.FloatTensor:
        """RNN encoder decoder loss function.
        Args:
            prediction (torch.FloatTensor): tensor of shape
                `batch_size, seq_len, vocab_size`.
            target (torch.LongTensor): tensor of shape `batch_size, seq_len`.
        Returns:
            torch.FloatTensor: loss value.
        """
        seq_len = target.size(1) - 1

        prediction = prediction.narrow(1, 0, seq_len).flatten(0, 1)
        target = target.narrow(1, 1, seq_len).flatten()

        return nn.functional.cross_entropy(prediction, target, ignore_index=0)

    @staticmethod
    def get_learnable_parameters(model, **kwargs):
        return model.parameters()

    def save(self, dump_name: str):
        dump = {"model_state_dict": self.state_dict(), "hparams": self.hparams}
        torch.save(dump, dump_name)

    @staticmethod
    def load(dump_name: str) -> "RNN":
        dump = torch.load(dump_name, map_location="cpu")
        model = RNN(**dump["hparams"])
        model.load_state_dict(dump["model_state_dict"])

        return model
