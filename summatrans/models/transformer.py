from typing import Tuple

import torch
from summatrans.decoding import BeamSearch
from summatrans.modules.transformer import (
    PositionalEncoding,
    TransformerDecoder,
    TransformerEncoder,
)
from torch import nn


class Transformer(nn.Module):
    def __init__(
        self,
        encoder_embeddings: Tuple[int, int],
        decoder_embeddings: Tuple[int, int],
        shared_embeddings: bool = True,
        attention: str = "linear",
        hidden_size: int = 512,
        num_layers: int = 6,
        num_heads: int = 8,
        max_seq_len: int = 512,
        dropout: float = 0.1,
        **kwargs,
    ):
        super().__init__()

        assert hidden_size == encoder_embeddings[1] == decoder_embeddings[1]

        pos_embs = PositionalEncoding(hidden_size, dropout, max_seq_len)

        if shared_embeddings:
            assert encoder_embeddings[0] == decoder_embeddings[0]
            embs = nn.Embedding(*encoder_embeddings, padding_idx=0)
            self.enc_embs = nn.Sequential(embs, pos_embs)
            self.dec_embs = nn.Sequential(embs, pos_embs)
        else:
            enc_embs = nn.Embedding(*encoder_embeddings, padding_idx=0)
            self.enc_embs = nn.Sequential(enc_embs, pos_embs)
            dec_embs = nn.Embedding(*decoder_embeddings, padding_idx=0)
            self.dec_embs = nn.Sequential(dec_embs, pos_embs)

        self.register_buffer("ar_mask", torch.ones(max_seq_len, max_seq_len).tril())

        vocab_size = decoder_embeddings[0]
        module_args = attention, num_layers, hidden_size, num_heads, dropout
        self.encoder = TransformerEncoder(*module_args)
        self.decoder = TransformerDecoder(*module_args)
        self.out_to_vocab = nn.Linear(hidden_size, vocab_size)

        self.max_seq_len = max_seq_len
        self.hparams = {
            "encoder_embeddings": encoder_embeddings,
            "decoder_embeddings": decoder_embeddings,
            "shared_embeddings": shared_embeddings,
            "attention": attention,
            "hidden_size": hidden_size,
            "num_layers": num_layers,
            "num_heads": num_heads,
            "max_seq_len": max_seq_len,
            "dropout": dropout,
        }

    def forward(self, source: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        n, _ = source.shape
        _, s = target.shape

        source_emb = self.enc_embs(source)
        target_emb = self.dec_embs(target)

        self_attention_mask = self.ar_mask[:s, :s].repeat(n, 1, 1)
        source_mask = torch.ne(source, 0)
        target_mask = torch.ne(target, 0)

        memory = self.encoder(source_emb, x_mask=source_mask)
        state = self.decoder(
            target_emb,
            memory,
            self_attention_mask,
            x_mask=target_mask,
            memory_mask=source_mask,
        )

        return self.out_to_vocab(state)

    @torch.no_grad()
    def generate(
        self,
        source: torch.Tensor,
        limit: int = None,
        bos_token: int = 1,
        eos_token: int = 2,
    ) -> torch.LongTensor:
        if limit is None:
            limit = self.max_seq_len
        else:
            assert limit <= self.max_seq_len

        batch_size, _ = source.shape
        device = source.device

        source = source[:, : self.max_seq_len, ...]
        encoder_emb = self.enc_embs(source)
        memory_mask = source.ne(0)
        memory = self.encoder(encoder_emb, x_mask=memory_mask)

        prediction = torch.full(
            (batch_size, 1), bos_token, dtype=torch.long, device=device
        )

        for i in range(1, limit):
            _, s = prediction.shape
            self_attention_mask = self.ar_mask[:s, :s].repeat(batch_size, 1, 1)

            decoder_emb = self.dec_embs(prediction)
            decoder_repr = self.decoder(
                decoder_emb, memory, self_attention_mask, memory_mask=memory_mask
            )
            distribution = self.out_to_vocab(decoder_repr[:, -1:]).argmax(-1)
            prediction = torch.cat((prediction, distribution), dim=1)

            if torch.sum(prediction == eos_token, 1, dtype=torch.bool).all():
                break

        return prediction

    @torch.no_grad()
    def generate_recurrent(
        self,
        source: torch.Tensor,
        limit: int = None,
        bos_token: int = 1,
        eos_token: int = 2,
    ) -> torch.LongTensor:
        if limit is None:
            limit = self.max_seq_len
        else:
            assert limit <= self.max_seq_len

        batch_size, _ = source.shape
        device = source.device

        source = source[:, : self.max_seq_len, ...]
        encoder_emb = self.enc_embs(source)
        memory_mask = source.ne(0)
        memory = self.encoder(encoder_emb, x_mask=memory_mask)

        prediction = torch.full(
            (batch_size, 1), bos_token, dtype=torch.long, device=device
        )
        state = None

        for i in range(1, limit):
            _, s = prediction.shape

            decoder_emb = self.dec_embs(prediction)[:, -1]
            decoder_repr, state = self.decoder.recurrent(
                decoder_emb, memory, memory_mask, state
            )
            distribution = self.out_to_vocab(decoder_repr).argmax(-1)
            prediction = torch.cat((prediction, distribution[:, None]), dim=1)

            if torch.sum(prediction == eos_token, 1, dtype=torch.bool).all():
                break

        return prediction

    @torch.no_grad()
    def generate_beamsearch(
        self,
        source: torch.Tensor,
        beam_size: int = 5,
        limit: int = None,
        bos_token: int = 1,
        eos_token: int = 2,
    ) -> torch.LongTensor:
        if limit is None:
            limit = self.max_seq_len
        else:
            assert limit <= self.max_seq_len

        batch_size, _ = source.shape
        device = source.device

        source = source[:, : self.max_seq_len, ...]
        encoder_emb = self.enc_embs(source)
        memory_mask = source.ne(0)
        memory = self.encoder(encoder_emb, x_mask=memory_mask)

        prediction = torch.full(
            (batch_size, 1), bos_token, dtype=torch.long, device=device
        )
        beam_search = BeamSearch(prediction, beam_size, eos_token)

        decoder_emb = self.dec_embs(prediction)[:, -1]
        decoder_repr, state = self.decoder.recurrent(
            decoder_emb, memory, memory_mask, None
        )
        logits = self.out_to_vocab(decoder_repr)
        prediction, state = beam_search.update(logits, state)

        memory_mask = memory_mask.repeat_interleave(beam_size, dim=0)
        memory = memory.repeat_interleave(beam_size, dim=0)

        for i in range(2, limit):
            decoder_emb = self.dec_embs(prediction)[:, -1]
            decoder_repr, state = self.decoder.recurrent(
                decoder_emb, memory, memory_mask, state
            )
            logits = self.out_to_vocab(decoder_repr)
            prediction, state = beam_search.update(logits, state)

            if beam_search.generated():
                break

        return beam_search.get_prediction()

    @staticmethod
    def criterion(
        prediction: torch.FloatTensor, target: torch.LongTensor
    ) -> torch.FloatTensor:
        """Transformer encoder decoder loss function.
        Args:
            prediction (torch.FloatTensor): tensor of shape
                `batch_size, seq_len, vocab_size`.
            target (torch.LongTensor): tensor of shape `batch_size, seq_len`.
        Returns:
            torch.FloatTensor: loss value.
        """
        seq_len = target.size(1) - 1

        prediction = prediction.narrow(1, 0, seq_len).flatten(0, 1)
        target = target.narrow(1, 1, seq_len).flatten()

        return nn.functional.cross_entropy(prediction, target, ignore_index=0)

    @staticmethod
    def get_learnable_parameters(model, **kwargs):
        return model.parameters()

    def save(self, dump_name: str):
        dump = {"model_state_dict": self.state_dict(), "hparams": self.hparams}
        torch.save(dump, dump_name)

    @staticmethod
    def load(dump_name: str) -> "Transformer":
        dump = torch.load(dump_name, map_location="cpu")
        model = Transformer(**dump["hparams"])
        model.load_state_dict(dump["model_state_dict"])

        return model
