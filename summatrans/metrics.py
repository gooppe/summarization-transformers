import re
from statistics import mean
from typing import Dict, List

from rouge import Rouge

dr = re.compile(r"^\.+$")


def rouge_metrics(hyps: List[str], refs: List[str]) -> Dict[str, float]:
    rouge = Rouge()
    # Filter single dot valued hypotesis:
    hyps, refs = zip(*((h, r) for h, r in zip(hyps, refs) if not dr.match(h)))
    scores = rouge.get_scores(hyps, refs, avg=True, ignore_empty=True)
    scores = {f"{metric}_f": value["f"] for metric, value in scores.items()}
    scores["rouge-m_f"] = mean(scores.values())
    return scores
