import torch

from typing import Optional


def reorder_cache(cache, indices: torch.Tensor):
    if isinstance(cache, (list, tuple)):
        return type(cache)(reorder_cache(c, indices) for c in cache)
    else:
        # batch * beam, other
        batch_size, beam_size = indices.shape
        cache = cache.view(*indices.shape, *cache.shape[1:])
        gather_indices = indices[(...,) + (None,) * (len(cache.shape) - 2)]
        gather_indices = gather_indices.expand_as(cache)
        return torch.gather(cache, 1, gather_indices).view(-1, *cache.shape[2:])


def repeat_interleave_cache(cache, n, dim):
    if isinstance(cache, (list, tuple)):
        return type(cache)(repeat_interleave_cache(c, n, dim) for c in cache)
    else:
        # batch * beam, other
        return cache.repeat_interleave(n, dim)


class BeamSearch:
    def __init__(
        self, initial: torch.Tensor, beam_size: int = 5, trailing_token: int = 2
    ):
        # batch, beam
        self.score: torch.Tensor = None
        # batch, beam, seq
        self.candidates = initial.view(-1, 1, 1).repeat(1, beam_size, 1)
        self.beam_size = beam_size
        self.trailing_token = trailing_token

    def update(self, logits: torch.Tensor, cache: Optional[torch.Tensor] = None):
        logits = torch.log_softmax(logits, -1)
        existed = self.candidates.size(-1)
        # First Update: logits shape = batch_size, vocab_size
        if self.score is None:
            batch_size, vocab_size = logits.shape
            # batch, beam
            self.score, indices = torch.topk(logits, self.beam_size)
            self.candidates = torch.cat((self.candidates, indices[:, :, None]), dim=-1)
            candidates = self.candidates.view(batch_size * self.beam_size, -1)

            if cache is not None:
                return candidates, repeat_interleave_cache(cache, self.beam_size, dim=0)
            else:
                return candidates

        # Follow Updates: logits shape = batch_size * beam_size, vocab_size
        batch_beam_size, vocab_size = logits.shape
        # batch * beam, vocab
        rescore = (self.score.view(-1, 1) * existed + logits) / (existed + 1)
        # batch, beam * vocab
        rescore = rescore.view(-1, self.beam_size * vocab_size)
        # batch, beam
        self.score, indices = torch.topk(rescore, self.beam_size)
        # batch, beam, seq
        beam_indices = indices // vocab_size
        # batch, beam
        tokens = indices % vocab_size
        gather_indices = beam_indices[:, :, None].expand_as(self.candidates)
        candidates = torch.gather(self.candidates, 1, gather_indices)
        # batch, beam, seq + 1
        self.candidates = torch.cat((candidates, tokens[:, :, None]), dim=-1)
        candidates = self.candidates.view(batch_beam_size, -1)

        if cache is not None:
            return candidates, reorder_cache(cache, beam_indices)
        else:
            return candidates

    def generated(self):
        trailed = self.candidates[:, 0] == self.trailing_token
        return torch.sum(trailed, 1, dtype=bool).all()

    def get_prediction(self):
        return self.candidates[:, 0]
