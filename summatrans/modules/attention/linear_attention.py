from typing import Optional, Tuple

import torch
from torch import nn

from ..causal_product import causal_dot_product


def elu_feature_map(x: torch.Tensor):
    return nn.functional.elu(x) + 1


def causal_linear(query, key, value):
    query = query.permute(0, 2, 1, 3).contiguous()
    key = key.permute(0, 2, 1, 3).contiguous()
    value = value.permute(0, 2, 1, 3).contiguous()
    value_new = causal_dot_product(query, key, value)
    return value_new.permute(0, 2, 1, 3).contiguous()


class LinearAttention(nn.Module):
    def __init__(self, eps: float = 1e-6):
        super().__init__()
        self.eps = eps

    def forward(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        attention_mask: torch.Tensor = None,
        query_mask: torch.Tensor = None,
        key_mask: torch.Tensor = None,
    ) -> torch.Tensor:
        query = elu_feature_map(query)
        key = elu_feature_map(key)

        if attention_mask is not None and not torch.all(attention_mask.bool()):
            raise RuntimeError(
                "LinearAttention does not support arbitrary attention masks"
            )

        if key_mask is not None:
            key = key * key_mask[:, :, None, None].float()

        logits = torch.einsum("nshd,nshm->nhmd", key, value)
        normalizer = 1 / (
            torch.einsum("nlhd,nhd->nlh", query, key.sum(dim=1)) + self.eps
        )
        attention = torch.einsum("nlhd,nhmd,nlh->nlhm", query, logits, normalizer)

        return attention.contiguous()

    def cross_recurrent(
        self,
        query: torch.Tensor,
        keys: torch.Tensor,
        values: torch.Tensor,
        key_mask: Optional[torch.Tensor] = None,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        query = elu_feature_map(query)

        if state is None:
            keys = elu_feature_map(keys)
            if key_mask is not None:
                keys = keys * key_mask[:, :, None, None].float()
            s = torch.einsum("nshd,nshm->nhmd", keys, values)
            z = keys.sum(dim=1)
        else:
            s, z = state

        qz = 1 / (torch.einsum("nhd,nhd->nh", query, z) + self.eps)
        v = torch.einsum("nhd,nhmd,nh->nhm", query, s, qz)

        return v.contiguous(), [s, z]


class CausalLinearAttention(nn.Module):
    def __init__(self, eps: float = 1e-6):
        super().__init__()
        self.eps = eps

    def forward(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        attention_mask: torch.Tensor = None,
        query_mask: torch.Tensor = None,
        key_mask: torch.Tensor = None,
    ) -> torch.Tensor:
        query = elu_feature_map(query)
        key = elu_feature_map(key)

        if attention_mask is not None:
            attention_mask = attention_mask.bool()
            if attention_mask.triu(1).any():
                raise RuntimeError(
                    "CausalLinearAttention only supports full lower triangular masks"
                )

        if key_mask is not None:
            key = key * key_mask[:, :, None, None].float()

        query, key = self._make_sizes_compatible(query, key)
        logits = 1 / (torch.einsum("nlhi,nlhi->nlh", query, key.cumsum(1)) + self.eps)
        value = causal_linear(query, key, value)

        return value * logits[:, :, :, None]

    def self_recurrent(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        query = elu_feature_map(query)
        key = elu_feature_map(key)

        n, h, d = query.shape
        _, _, m = value.shape

        if state is None:
            si = query.new_zeros((n, h, d, m))
            zi = query.new_zeros((n, h, d))
        else:
            si, zi = state

        if len(si) != n:
            raise ValueError("The batch size changed during iteration")

        zi += key
        si += torch.einsum("nhd,nhm->nhdm", key, value)

        z = 1.0 / (torch.einsum("nhd,nhd->nh", query, zi) + self.eps)
        v = torch.einsum("nhd,nhdm,nh->nhm", query, si, z)

        return v, [si, zi]

    @staticmethod
    def _make_sizes_compatible(query, key):
        n, l, h, e = query.shape
        _, s, _, _ = key.shape
        if l == s:
            return query, key

        if l < s:
            return query, key[:, :l, :, :]

        if l > s:
            return query, torch.cat([key, key.new_zeros(n, l - s, h, e)], dim=1)
