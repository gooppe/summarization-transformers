from math import sqrt
from typing import Optional, Tuple

import torch
from torch import nn


class FullAttention(nn.Module):
    def __init__(self, dropout: float = 0.1):
        super().__init__()

        self.dropout = nn.Dropout(dropout)

    def forward(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        attention_mask: torch.Tensor = None,
        query_mask: torch.Tensor = None,
        key_mask: torch.Tensor = None,
    ) -> torch.Tensor:
        n, l, h, e = query.shape
        _, s, _, d = value.shape

        scaling_factor = 1.0 / sqrt(e)
        query = query * scaling_factor
        logits = torch.einsum("nlhe,nshe->nhls", query, key)

        if attention_mask is not None:
            logits.masked_fill_(~attention_mask[:, None].bool(), -float("inf"))
        if key_mask is not None:
            logits.masked_fill_(~key_mask[:, None, None].bool(), -float("inf"))

        attention = self.dropout(torch.softmax(logits, dim=-1))
        return torch.einsum("nhls,nshd->nlhd", attention, value).contiguous()

    def self_recurrent(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        n, h, e = query.shape
        _, _, d = value.shape
        scaling_factor = 1.0 / sqrt(e)

        if state is None:
            keys = key[:, :, None, :]
            values = value[:, :, None, :]
        else:
            keys, values = state
            keys = torch.cat([keys, key[:, :, None, :]], dim=2)
            values = torch.cat([values, value[:, :, None, :]], dim=2)

        logits = torch.einsum("nhe,nhse->nhs", query, keys)
        attention = self.dropout(torch.softmax(scaling_factor * logits, dim=-1))
        result = torch.einsum("nhs,nhsd->nhd", attention, values).contiguous()

        return result, (keys, values)

    def cross_recurrent(
        self,
        query: torch.Tensor,
        keys: torch.Tensor,
        values: torch.Tensor,
        key_mask: Optional[torch.Tensor] = None,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        n, h, e = query.shape
        scaling_factor = 1.0 / sqrt(e)

        if state is not None:
            keys, values = state

        logits = torch.einsum("nhe,nshe->nhs", query, keys)
        if key_mask is not None:
            logits.masked_fill_(~key_mask[:, None, :].bool(), -float("inf"))

        attention = self.dropout(torch.softmax(scaling_factor * logits, dim=-1))
        result = torch.einsum("nhs,nshd->nhd", attention, values).contiguous()

        return result, (keys, values)
