from typing import Optional, Tuple

import torch
from torch import nn


class MultiHeadAttention(nn.Module):
    def __init__(
        self,
        attention: nn.Module,
        hidden_size: int,
        num_heads: int,
        dropout: float = 0.1,
    ):
        super().__init__()

        assert hidden_size % num_heads == 0
        self.head_size = hidden_size // num_heads
        self.num_heads = num_heads

        self.attention = attention
        self.q_proj = nn.Linear(hidden_size, hidden_size)
        self.k_proj = nn.Linear(hidden_size, hidden_size)
        self.v_proj = nn.Linear(hidden_size, hidden_size)
        self.fully_connected = nn.Sequential(
            nn.Linear(hidden_size, hidden_size), nn.Dropout(dropout)
        )

    def forward(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        attention_mask: torch.Tensor = None,
        query_mask: torch.Tensor = None,
        key_mask: torch.Tensor = None,
    ):
        n, l, _ = query.shape
        _, s, _ = key.shape
        h = self.num_heads

        query = self.q_proj(query).view(n, l, h, -1)
        key = self.k_proj(key).view(n, s, h, -1)
        value = self.v_proj(value).view(n, s, h, -1)

        attention = self.attention(
            query, key, value, attention_mask, query_mask, key_mask
        ).view(n, l, -1)

        return self.fully_connected(attention)

    def self_recurrent(
        self,
        query: torch.Tensor,
        key: torch.Tensor,
        value: torch.Tensor,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:

        n, d = query.shape
        h = self.num_heads

        query = self.q_proj(query).view(n, h, -1)
        key = self.k_proj(key).view(n, h, -1)
        value = self.v_proj(value).view(n, h, -1)

        result, state = self.attention.self_recurrent(query, key, value, state)
        result = self.fully_connected(result.view(n, -1))

        return result, state

    def cross_recurrent(
        self,
        query: torch.Tensor,
        keys: torch.Tensor,
        values: torch.Tensor,
        key_mask: Optional[torch.Tensor] = None,
        state: Optional[Tuple[torch.Tensor, torch.Tensor]] = None,
    ) -> Tuple[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
        n, d = query.shape
        h = self.num_heads

        query = self.q_proj(query).view(n, h, -1)

        if state is None:
            _, s, _ = keys.shape
            keys = self.k_proj(keys).view(n, s, h, -1)
            values = self.v_proj(values).view(n, s, h, -1)
        else:
            keys = None
            values = None

        result, state = self.attention.cross_recurrent(
            query, keys, values, key_mask, state
        )
        result = self.fully_connected(result.view(n, -1))

        return result, state
