from typing import Tuple

import torch
from torch import nn

from .attention import (
    CausalLinearAttention,
    FullAttention,
    LinearAttention,
    MultiHeadAttention,
)


class TransformerEncoderLayer(nn.Module):
    def __init__(
        self,
        attention: nn.Module,
        hidden_size: int,
        num_heads: int,
        dropout: float = 0.1,
    ):
        super().__init__()

        self.attention = MultiHeadAttention(attention, hidden_size, num_heads, dropout)
        self.position_wise = nn.Sequential(
            nn.Linear(hidden_size, hidden_size * 4),
            nn.GELU(),
            nn.Linear(hidden_size * 4, hidden_size),
            nn.Dropout(dropout),
        )
        self.ln_1 = nn.LayerNorm(hidden_size)
        self.ln_2 = nn.LayerNorm(hidden_size)

    def forward(
        self,
        x: torch.Tensor,
        attention_mask: torch.Tensor = None,
        x_mask: torch.Tensor = None,
    ) -> torch.Tensor:
        x = self.ln_1(x + self.attention(x, x, x, attention_mask, x_mask, x_mask))
        x = self.ln_2(x + self.position_wise(x))

        return x


class TransformerEncoder(nn.Module):
    def __init__(
        self,
        attention: str,
        num_layers: int,
        hidden_size: int,
        num_heads: int,
        dropout: float = 0.1,
    ):
        super().__init__()

        if attention == "linear":
            attention = LinearAttention()
        elif attention == "softmax":
            attention = FullAttention(dropout)
        else:
            raise ValueError(
                f"Attention must be `linear` or `softmax`, got {attention}"
            )

        layer_args = attention, hidden_size, num_heads, dropout
        self.layers = nn.ModuleList(
            [TransformerEncoderLayer(*layer_args) for _ in range(num_layers)]
        )

    def forward(
        self,
        x: torch.Tensor,
        attention_mask: torch.Tensor = None,
        x_mask: torch.Tensor = None,
    ) -> torch.Tensor:
        for layer in self.layers:
            x = layer(x, attention_mask, x_mask)

        return x


class TransformerDecoderLayer(nn.Module):
    def __init__(
        self,
        self_attention: nn.Module,
        cross_attention: nn.Module,
        hidden_size: int,
        num_heads: int,
        dropout: float = 0.1,
    ):
        super().__init__()

        attention_args = hidden_size, num_heads, dropout
        self.self_attention = MultiHeadAttention(self_attention, *attention_args)
        self.cross_attention = MultiHeadAttention(cross_attention, *attention_args)
        self.position_wise = nn.Sequential(
            nn.Linear(hidden_size, hidden_size * 4),
            nn.GELU(),
            nn.Linear(hidden_size * 4, hidden_size),
            nn.Dropout(dropout),
        )
        self.ln_1 = nn.LayerNorm(hidden_size)
        self.ln_2 = nn.LayerNorm(hidden_size)
        self.ln_3 = nn.LayerNorm(hidden_size)

    def forward(
        self,
        x,
        memory,
        self_attention_mask,
        cross_attention_mask=None,
        x_mask=None,
        memory_mask=None,
    ) -> torch.Tensor:
        x = self.ln_1(
            x + self.self_attention(x, x, x, self_attention_mask, x_mask, x_mask)
        )
        x = self.ln_2(
            x
            + self.cross_attention(
                x, memory, memory, cross_attention_mask, x_mask, memory_mask
            )
        )
        x = self.ln_3(x + self.position_wise(x))

        return x

    def recurrent(
        self,
        x,
        memory,
        memory_mask=None,
        state=None,
    ) -> Tuple[torch.Tensor, Tuple]:
        self_state, cross_state = state or (None, None)
        x_, self_state = self.self_attention.self_recurrent(x, x, x, self_state)
        x = self.ln_1(x + x_)
        x_, cross_state = self.cross_attention.cross_recurrent(
            x, memory, memory, memory_mask, cross_state
        )
        x = self.ln_2(x + x_)
        x = self.ln_3(x + self.position_wise(x))
        return x, (self_state, cross_state)


class TransformerDecoder(nn.Module):
    def __init__(
        self,
        attention: str,
        num_layers: int,
        hidden_size: int,
        num_heads: int,
        dropout: float = 0.1,
    ):
        super().__init__()

        if attention == "linear":
            self_att = CausalLinearAttention()
            cross_att = LinearAttention()
        elif attention == "softmax":
            self_att = cross_att = FullAttention(dropout)
        else:
            raise ValueError(
                f"Attention must be `linear` or `softmax`, got {attention}"
            )

        layer_args = self_att, cross_att, hidden_size, num_heads, dropout
        self.layers = nn.ModuleList(
            [TransformerDecoderLayer(*layer_args) for _ in range(num_layers)]
        )

    def forward(
        self,
        x,
        memory,
        self_attention_mask=None,
        cross_attention_mask=None,
        x_mask=None,
        memory_mask=None,
    ) -> torch.Tensor:
        for layer in self.layers:
            x = layer(
                x,
                memory,
                self_attention_mask,
                cross_attention_mask,
                x_mask,
                memory_mask,
            )

        return x

    def recurrent(
        self,
        x,
        memory,
        memory_mask=None,
        state=None,
    ) -> Tuple[torch.Tensor, Tuple]:
        if state is None:
            state = [None for _ in self.layers]

        for i, layer in enumerate(self.layers):
            x, s = layer.recurrent(x, memory, memory_mask, state[i])
            state[i] = s

        return x, state


class PositionalEncoding(nn.Module):
    def __init__(self, hidden_size: int, dropout=0.1, max_len=512):
        super(PositionalEncoding, self).__init__()

        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, hidden_size)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.pow(
            1e4, -torch.arange(0, hidden_size, 2).float() / hidden_size
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer("pe", pe)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = x + self.pe[:, : x.size(1), :]
        return self.dropout(x)
