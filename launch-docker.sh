#!/usr/bin/env bash

# Usage: ./launch-docker.sh data/ dumpdir/

sudo docker run -it --rm --gpus all --dns 8.8.8.8 -v $1:/data -v $2:/summarization-transformers/wandb summatrans