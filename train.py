import argparse
import os
from typing import Tuple, Union

import torch
import wandb
import youtokentome
from torch import distributed as dist
from torch import nn
from torch.optim import Adam
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm

from summatrans.datasets import IterableNewsDataset
from summatrans.metrics import rouge_metrics
from summatrans.models import Transformer
from summatrans.utils import get_cosine_schedule_with_warmup


_available_models = {"transformer": Transformer}


def load_news_data(
    tokenizer: Union[str, youtokentome.BPE],
    data_dir: str,
    part: str,
    batch_size: int,
    num_workers: int,
    max_seq_len: int,
    **kwargs,
) -> Tuple[IterableNewsDataset, DataLoader]:
    if tokenizer is None:
        tokenizer = os.path.join(data_dir, "tokenizer.model")
    if isinstance(tokenizer, str):
        tokenizer = youtokentome.BPE(tokenizer)

    dataset = IterableNewsDataset(tokenizer, data_dir, part, batch_size, max_seq_len)
    data_loader = DataLoader(
        dataset,
        None,
        num_workers=num_workers,
        collate_fn=dataset.collate,
    )

    return tokenizer, dataset, data_loader


def train(**kwargs):
    local_rank = kwargs["local_rank"]
    epochs = kwargs["epochs"]

    is_main_rank = local_rank in [-1, 0]
    ignored_tokens = [0, 1, 2]

    if local_rank > -1:
        # Initialize distributed training if necessary
        dist.init_process_group(backend="nccl")
        torch.cuda.set_device(local_rank)
        device = torch.device("cuda", local_rank)
    else:
        device = torch.device("cuda")

    tokenizer, train_data, train_loader = load_news_data(None, part="train", **kwargs)
    total_steps = len(train_loader) * kwargs["epochs"]
    total_steps //= kwargs["batch_size"] * kwargs["gradient_accumulation"]

    # Load test data and initialize loggers only on main rank
    if is_main_rank:
        _, test_data, test_loader = load_news_data(tokenizer, part="test", **kwargs)

        global_step = 0
        pbar = tqdm(total=total_steps)
        wandb.init(project=kwargs["project_name"], config=kwargs)
        wandb.config.vocab_size = tokenizer.vocab_size()

    # Load model, optimizer and scheduler
    Model = _available_models[kwargs["model"]]
    embs = (tokenizer.vocab_size(), kwargs["hidden_size"])
    model = Model(embs, embs, **kwargs).to(device)
    if local_rank != -1:
        model = nn.parallel.DistributedDataParallel(
            model, [local_rank], local_rank, find_unused_parameters=True
        )
    parameters = Model.get_learnable_parameters(model)
    optimizer = Adam(parameters, kwargs["lr"], betas=(0.9, 0.98), eps=1e-9)
    scheduler = get_cosine_schedule_with_warmup(
        optimizer, kwargs["warmup"], total_steps
    )

    if is_main_rank:
        wandb.config.num_params = sum(p.numel() for p in model.module.parameters())
        print(wandb.config.num_params)

    # Train loop
    for epoch in range(1, epochs + 1):
        model.train()
        optimizer.zero_grad()
        for i, batch in enumerate(train_loader, 1):
            source, target = [t.to(device) for t in batch]

            prediction = model(source, target)
            loss = Model.criterion(prediction, target)
            loss.backward()

            if i % kwargs["gradient_accumulation"] != 0:
                continue

            nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            optimizer.step()
            scheduler.step()
            optimizer.zero_grad()

            # Train Logging
            if is_main_rank:
                global_step += 1
                loss = float(loss.detach())
                wandb.log({"train_loss": loss, "lr": scheduler.get_last_lr()[0]})
                pbar.desc = f"Ep:{epoch}/{epochs};Loss:{loss:.3f}"
                pbar.update()

            # Test Logging
            if is_main_rank and global_step % kwargs["log_every"] == 0:
                model.eval()
                src, trg, src_enc, trg_enc = test_data.get_random_test_sample()
                pred = model.module.generate(src_enc.to(device))
                pred = tokenizer.decode(pred.tolist(), ignored_tokens)[0]

                sample = wandb.Table(
                    ["Source", "Target", "Prediction"], [[src, trg, pred]]
                )

                pbar.write(f"Source: {src};\nTarget: {trg};\nPred: {pred}\n")
                wandb.log({"sample": sample})
                model.train()

        if is_main_rank:
            # Calculate metrics
            model.eval()
            predictions, targets = [], []
            for batch in tqdm(test_loader, desc="Evaluating"):
                source, target = [t.to(device) for t in batch]
                prediction = model.module.generate(source)
                prediction = tokenizer.decode(prediction.tolist(), ignored_tokens)
                target = tokenizer.decode(target.tolist(), ignored_tokens)
                predictions += prediction
                targets += target

            metrics = rouge_metrics(predictions, targets)
            metrics["epoch"] = epoch
            wandb.log(metrics)

            # Save model
            dump_dir = os.path.join(wandb.run.dir, f"model_{epoch}_ep.pth")
            model.module.save(dump_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # Train args
    parser.add_argument("--local_rank", type=int, default=-1)
    parser.add_argument("--data-dir", type=str, default="/data")
    parser.add_argument("--project-name", type=str, default="summarization")
    parser.add_argument("--log-every", type=int, default=500)
    parser.add_argument("--batch-size", type=int, default=32)
    parser.add_argument("--gradient-accumulation", type=int, default=1)
    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--lr", type=float, default=3e-4)
    parser.add_argument("--warmup", type=int, default=5000)
    parser.add_argument("--num-workers", type=int, default=1)

    # Model args
    parser.add_argument(
        "--model", type=str, choices=_available_models.keys(), required=True
    )
    parser.add_argument("--shared-embeddings", action="store_true")
    parser.add_argument("--attention", type=str, default="linear")
    parser.add_argument("--hidden-size", type=int, default=512)
    parser.add_argument("--num-layers", type=int, default=6)
    parser.add_argument("--num-heads", type=int, default=8)
    parser.add_argument("--max-seq-len", type=int, default=256)
    parser.add_argument("--dropout", type=float, default=0.1)

    args = parser.parse_args()

    train(**vars(args))
