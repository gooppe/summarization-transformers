# Summarization Transformers

Sample command for distributed training:

```bash
python -m torch.distributed.launch --nproc_per_node=2 train.py --batch-size 48 --max-seq-len 128
```

More info about training and evaluation can be obtained using `--help` argument.