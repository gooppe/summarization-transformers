FROM pytorch/pytorch:1.7.1-cuda11.0-cudnn8-devel


ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    cmake git \
    build-essential ca-certificates

WORKDIR /summarization-transformers
COPY requirements.txt /summarization-transformers
RUN pip install --upgrade pip \
    && pip install -r requirements.txt

RUN python -m nltk.downloader wordnet

COPY . /summarization-transformers
RUN pip install -e .

CMD [ "bash" ]