import sys
from functools import lru_cache
from subprocess import DEVNULL, call

from setuptools import find_packages, setup

try:
    import torch
    from torch.utils.cpp_extension import BuildExtension, CppExtension
except ImportError as e:
    raise ImportError(name=e.name, path=e.path) from e


@lru_cache()
def _get_extra_compile_args():
    base_args = ["-fopenmp", "-ffast-math"]

    if sys.platform == "darwin":
        return ["-Xpreprocessor"] + base_args
    else:
        return base_args


@lru_cache(None)
def cuda_toolkit_available():
    try:
        call(["nvcc"], stdout=DEVNULL, stderr=DEVNULL)
        return True
    except FileNotFoundError:
        return False


def get_extensions():
    extentions = [
        CppExtension(
            "summatrans.modules.causal_product.causal_product_cpu",
            sources=["summatrans/modules/causal_product/causal_product_cpu.cpp"],
            extra_compile_args=_get_extra_compile_args(),
        ),
    ]

    if cuda_toolkit_available():
        from torch.utils.cpp_extension import CUDAExtension

        extentions += [
            CUDAExtension(
                "summatrans.modules.causal_product.causal_product_cuda",
                sources=["summatrans/modules/causal_product/causal_product_cuda.cu"],
                extra_compile_args=["-arch=compute_50"],
            )
        ]

    return extentions


setup(
    name="summatrans",
    version="0.0.1",
    packages=find_packages(
        include=["summatrans", "summatrans.*"],
    ),
    description="Summarization Transformers",
    python_requires=">=3.5.0",
    ext_modules=get_extensions(),
    cmdclass={"build_ext": BuildExtension},
    install_requires=["torch>=1.4.0", "youtokentome>=1.0.6"],
)
