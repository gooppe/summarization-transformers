from summatrans.modules.transformer import TransformerEncoder, TransformerDecoder
import torch


def test_TransformerEncoder_forward():
    n, l, h, d = 5, 10, 8, 64

    encoder = TransformerEncoder("linear", 3, d, h)
    tensor = torch.randn(n, l, d)
    attention_mask = torch.ones(n, l, l)
    tensor_mask = torch.ones(n, l).tril()
    output = encoder(tensor, attention_mask, tensor_mask)

    assert output.shape == (n, l, d)


def test_TransformerDecoder_forward():
    n, l, s, h, d = 5, 10, 15, 8, 64

    decoder = TransformerDecoder("linear", 3, d, h)
    tensor = torch.randn(n, l, d)
    memory = torch.randn(n, s, d)
    self_attention_mask = torch.ones(n, l, l).tril()
    cross_attention_mask = torch.ones(n, l, s)
    tensor_mask = torch.ones(n, l).tril()
    memory_mask = torch.ones(n, s).tril()
    output = decoder(
        tensor,
        memory,
        self_attention_mask,
        cross_attention_mask,
        tensor_mask,
        memory_mask,
    )

    assert output.shape == (n, l, d)


def test_TransformerDecoder_recurrent():
    n, s, h, d = 5, 15, 8, 64

    decoder = TransformerDecoder("softmax", 3, d, h).eval()
    tensor = torch.randn(n, 3, d)
    attention_mask = torch.ones(n, 3, 3).tril()

    memory = torch.randn(n, s, d)
    memory_mask = None

    output, state = decoder.recurrent(tensor[:, 0], memory, memory_mask)
    assert output.shape == (n, d)
    for s_s, c_s in state:
        assert s_s[0].shape == s_s[1].shape == (n, h, 1, d // h)
        assert c_s[0].shape == c_s[1].shape == (n, s, h, d // h)

    output, state = decoder.recurrent(tensor[:, 1], memory, memory_mask, state)
    assert output.shape == (n, d)
    for s_s, c_s in state:
        assert s_s[0].shape == s_s[1].shape == (n, h, 2, d // h)
        assert c_s[0].shape == c_s[1].shape == (n, s, h, d // h)

    non_rec_out = decoder(tensor, memory, attention_mask)
    assert torch.allclose(output, non_rec_out[:, 1], rtol=1e-3)
