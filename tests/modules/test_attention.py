import torch
from summatrans.modules.attention import (
    FullAttention,
    LinearAttention,
    CausalLinearAttention,
    MultiHeadAttention,
)


def test_FullAttention_forward():
    n, l, s, h, d = 5, 10, 15, 8, 64
    q = torch.randn(n, l, h, d)
    k = v = torch.randn(n, s, h, d)
    attention_mask = torch.tril(torch.ones(n, l, s))
    key_mask = torch.tril(torch.ones(n, s))

    attention = FullAttention()
    out = attention(q, k, v, attention_mask, key_mask=key_mask)
    assert out.shape == (n, l, h, d)
    assert not out.isnan().any()


def test_LinearAttention_forward():
    n, l, s, h, d = 5, 10, 15, 8, 64
    q = torch.randn(n, l, h, d)
    k = v = torch.randn(n, s, h, d)
    attention_mask = torch.ones(n, l, s)
    key_mask = torch.ones(n, s).tril()

    attention = LinearAttention()
    out = attention(q, k, v, attention_mask, key_mask=key_mask)
    assert out.shape == (n, l, h, d)
    assert not out.isnan().any()


def test_CausalLinearAttention_forward():
    n, l, s, h, d = 5, 10, 15, 8, 64
    q = torch.randn(n, l, h, d)
    k = v = torch.randn(n, s, h, d)
    attention_mask = torch.ones(n, l, s).tril()
    key_mask = torch.ones(n, s).tril()

    attention = CausalLinearAttention()
    out = attention(q, k, v, attention_mask, key_mask=key_mask)
    assert out.shape == (n, l, h, d)
    assert not out.isnan().any()


def test_MultiHeadAttention_forward():
    n, l, s, h, d = 5, 10, 15, 8, 64
    q = torch.randn(n, l, d)
    k = v = torch.randn(n, s, d)

    attention_mask = torch.ones(n, l, s).tril()
    key_mask = torch.ones(n, s).tril()

    attention = FullAttention()
    mh_attention = MultiHeadAttention(attention, d, h)
    out = mh_attention(q, k, v, attention_mask, key_mask=key_mask)
    assert out.shape == (n, l, d)
    assert not out.isnan().any()


def test_FullAttention_self_recurrent():
    n, h, d = 5, 8, 64
    q = torch.randn(n, 2, h, d)
    attention_mask = torch.ones(n, 2, 2).tril()
    attention = FullAttention().eval()

    out, state = attention.self_recurrent(q[:, 0], q[:, 0], q[:, 0])
    assert out.shape == (n, h, d)
    assert state[0].shape == (n, h, 1, d)
    assert state[1].shape == (n, h, 1, d)
    assert not out.isnan().any()

    out, state = attention.self_recurrent(q[:, 1], q[:, 1], q[:, 1], state)
    assert out.shape == (n, h, d)
    assert state[0].shape == (n, h, 2, d)
    assert state[1].shape == (n, h, 2, d)
    assert not out.isnan().any()

    non_rec_out = attention(q, q, q, attention_mask)
    assert torch.allclose(non_rec_out[:, -1], out)


def test_FullAttention_cross_recurrent():
    n, h, l, d = 5, 8, 10, 64
    q = torch.randn(n, h, d)
    k = v = torch.randn(n, l, h, d)
    key_mask = torch.ones(n, l).tril()

    attention = FullAttention()

    out, state = attention.cross_recurrent(q, k, v, key_mask)
    assert out.shape == (n, h, d)
    assert state[0].shape == (n, l, h, d)
    assert state[1].shape == (n, l, h, d)
    assert not out.isnan().any()

    out, state = attention.cross_recurrent(q, k, v, key_mask, state)
    assert out.shape == (n, h, d)
    assert state[0].shape == (n, l, h, d)
    assert state[1].shape == (n, l, h, d)
    assert not out.isnan().any()


def test_MultiHeadAttention_self_recurrent():
    n, h, d = 5, 8, 64
    q = torch.randn(n, 3, d)
    attention_mask = torch.ones(n, 3, 3).tril()

    attention = FullAttention()
    mh_attention = MultiHeadAttention(attention, d, h).eval()

    out, state = mh_attention.self_recurrent(q[:, 0], q[:, 0], q[:, 0])
    assert out.shape == (n, d)
    assert state[0].shape == state[1].shape == (n, h, 1, d // h)
    assert not out.isnan().any()

    out, state = mh_attention.self_recurrent(q[:, 1], q[:, 1], q[:, 1], state)
    assert out.shape == (n, d)
    assert state[0].shape == state[1].shape == (n, h, 2, d // h)
    assert not out.isnan().any()

    non_rec_out = mh_attention(q, q, q, attention_mask)
    assert torch.allclose(non_rec_out[:, 1], out, rtol=1e-04, atol=1e-3)


def test_MultiHeadAttention_cross_recurrent():
    n, h, l, d = 5, 8, 10, 64
    q = torch.randn(n, d)
    k = v = torch.randn(n, l, d)
    key_mask = torch.ones(n, l).tril()

    attention = FullAttention()
    mh_attention = MultiHeadAttention(attention, d, h)

    out, state = mh_attention.cross_recurrent(q, k, v, key_mask)
    assert out.shape == (n, d)
    assert state[0].shape == (n, l, h, d // h)
    assert state[1].shape == (n, l, h, d // h)
    assert not out.isnan().any()

    out, state = mh_attention.cross_recurrent(q, k, v, key_mask, state)
    assert out.shape == (n, d)
    assert state[0].shape == (n, l, h, d // h)
    assert state[1].shape == (n, l, h, d // h)
    assert not out.isnan().any()
