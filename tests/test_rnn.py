# from summatrans.models import RNN
# import torch

# rnn_kwargs = {
#     "encoder_embeddings": (100, 128),
#     "decoder_embeddings": (100, 128),
#     "hidden_size": 128,
#     "num_layers": 3,
# }


# def test_rnn_forward():
#     bs, s_l, t_l = 7, 15, 5

#     model = RNN(**rnn_kwargs)

#     source = torch.randint(100, (bs, s_l))
#     target = torch.randint(100, (bs, t_l))

#     distr = model(source, target)

#     assert distr.shape == (bs, t_l, 100)


# def test_rnn_generate():
#     bs, s_l, limit = 7, 15, 11

#     model = RNN(**rnn_kwargs)

#     source = torch.randint(100, (bs, s_l))
#     prediction = model.generate(source, limit)

#     assert prediction.shape == (bs, limit)
