import torch
from summatrans.models import Transformer


def test_Transformer_linear_forward():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "linear", d, 3)

    source = torch.randint(v, (n, l))
    target = torch.randint(v, (n, s))

    distribution = transformer(source, target)
    loss = transformer.criterion(distribution, target)

    assert distribution.shape == (n, s, v)
    assert float(loss) > 0


def test_Transformer_softmax_forward():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "softmax", d, 3)

    source = torch.randint(v, (n, l))
    target = torch.randint(v, (n, s))

    distribution = transformer(source, target)
    loss = transformer.criterion(distribution, target)

    assert distribution.shape == (n, s, v)
    assert float(loss) > 0


def test_Transformer_generate_greedy():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "linear", d, 3)

    source = torch.randint(v, (n, l))
    prediction = transformer.generate(source, limit=s)

    assert prediction.shape == (n, s)


def test_softmaxTransformer_generate_recurrent():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "softmax", d, 3).eval()

    source = torch.randint(v, (n, l))

    torch.manual_seed(42)
    prediction = transformer.generate(source, limit=s)

    torch.manual_seed(42)
    rec_pred = transformer.generate_recurrent(source, limit=s)

    assert rec_pred.shape == (n, s)
    assert torch.all(prediction == rec_pred)


def test_linearTransformer_generate_recurrent():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "linear", d, 3).eval()

    source = torch.randint(v, (n, l))

    torch.manual_seed(42)
    prediction = transformer.generate(source, limit=s)

    torch.manual_seed(42)
    rec_pred = transformer.generate_recurrent(source, limit=s)

    assert rec_pred.shape == (n, s)
    assert torch.all(prediction == rec_pred)


def test_softmaxTransformer_generate_beamsearch():
    n, l, s, d, v = 5, 10, 15, 64, 100
    transformer = Transformer((v, d), (v, d), False, "softmax", d, 3).eval()

    source = torch.randint(v, (n, l))

    prediction = transformer.generate_beamsearch(source, limit=s)

    assert prediction.shape == (n, s)
