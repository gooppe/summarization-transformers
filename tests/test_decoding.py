from summatrans.decoding import BeamSearch
import torch


def test_BeamSearch():
    batch_size, beam_size, vocab_size, limit = 8, 5, 100, 10
    initial = torch.ones(batch_size)
    beam_search = BeamSearch(initial, beam_size)
    prediction = beam_search.update(torch.randn(batch_size, vocab_size))

    for step in range(limit - 2):
        prediction = beam_search.update(torch.randn(prediction.size(0), vocab_size))

    prediction = beam_search.get_prediction()

    assert prediction.shape == (batch_size, limit)


def test_BeamSearch_cached():
    batch_size, beam_size, vocab_size, limit = 8, 5, 100, 10
    initial = torch.ones(batch_size)
    beam_search = BeamSearch(initial, beam_size)
    cache = torch.randn(batch_size, 3, 4, 5)
    prediction, cache = beam_search.update(torch.randn(batch_size, vocab_size), cache)

    for step in range(limit - 2):
        prediction, cache = beam_search.update(
            torch.randn(prediction.size(0), vocab_size), cache
        )

    prediction = prediction.view(batch_size, beam_size, -1)[:, 1]

    assert prediction.shape == (batch_size, limit)
