import argparse
import json
import os
from collections import defaultdict
from itertools import chain
from statistics import mean

import torch
from nltk.translate.bleu_score import corpus_bleu
from nltk.translate.meteor_score import meteor_score
from tqdm import tqdm
from youtokentome import BPE

from summatrans.datasets.utils import count_lines
from summatrans.metrics import rouge_metrics
from summatrans.models import Transformer

_ignored_tokens = [0, 1, 2]


def iter_data_part(filename: str):
    with open(filename) as file:
        for line in file:
            sample = json.loads(line)
            yield sample["text"], sample["headline"], sample["source"]


def eval_by_source(**kwargs):
    model = Transformer.load(kwargs["dump"])

    if kwargs["quantized"]:
        device = torch.device("cpu")
        model = torch.quantization.quantize_dynamic(
            model, {torch.nn.Linear}, dtype=torch.qint8
        )
    else:
        device = torch.device(kwargs["device"])

    model.to(device)
    model.eval()

    num_parameters = sum(p.numel() for p in model.parameters())
    print(f"Model dump have been loaded. Number of paramters: {num_parameters}.")

    bpe = BPE(os.path.join(kwargs["data_dir"], "tokenizer.model"))
    test_data_file = os.path.join(kwargs["data_dir"], "test.jsonl")
    test_data = iter_data_part(test_data_file)

    total_samples = count_lines(test_data_file)
    references = defaultdict(list)

    for text, headline, source in tqdm(
        test_data, total=total_samples, desc="Evaluating"
    ):
        text = torch.LongTensor(bpe.encode([text])).to(device)
        if kwargs["mode"] == "beamsearch":
            prediction = model.generate_beamsearch(text)
        elif kwargs["mode"] == "recurrent":
            prediction = model.generate_recurrent(text)
        else:
            prediction = model.generate(text)
        prediction = bpe.decode(prediction.tolist(), _ignored_tokens)[0]

        references[source].append((prediction, headline))

    rouge_scores = defaultdict(list)

    for source, hyps_refs in references.items():
        scores = rouge_metrics(*zip(*hyps_refs))
        for name, score in scores.items():
            rouge_scores[name].append(score)

        print(source, ":", scores)

    mean_scores = {name: mean(vals) for name, vals in rouge_scores.items()}
    print("Rouge Score (mean):", mean_scores)

    hyps, refs = zip(*chain(*references.values()))
    bleu_score = corpus_bleu([[r] for r in refs], hyps)
    print("Bleu Score (mean):", bleu_score)

    met_score = mean([meteor_score([r], h) for r, h in zip(refs, hyps)])
    print("Meteor Score (mean):", met_score)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--data-dir", type=str, default="/data")
    parser.add_argument("--device", type=str, default="cpu")
    parser.add_argument("--quantized", action="store_true")
    parser.add_argument(
        "mode", choices=["simple", "recurrent", "beamsearch", "quantized"]
    )
    parser.add_argument("dump", type=str)

    args = parser.parse_args()

    eval_by_source(**vars(args))
